%title: Clickhouse
%author: xavki

 ██████╗██╗     ██╗ ██████╗██╗  ██╗██╗  ██╗ ██████╗ ██╗   ██╗███████╗███████╗
██╔════╝██║     ██║██╔════╝██║ ██╔╝██║  ██║██╔═══██╗██║   ██║██╔════╝██╔════╝
██║     ██║     ██║██║     █████╔╝ ███████║██║   ██║██║   ██║███████╗█████╗  
██║     ██║     ██║██║     ██╔═██╗ ██╔══██║██║   ██║██║   ██║╚════██║██╔══╝  
╚██████╗███████╗██║╚██████╗██║  ██╗██║  ██║╚██████╔╝╚██████╔╝███████║███████╗
 ╚═════╝╚══════╝╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝  ╚═════╝ ╚══════╝╚══════╝

-----------------------------------------------------------------------

# Clickhouse - Monitoring

<br>

* add prometheus endpoint

```
echo '
<clickhouse>
 <prometheus>
    <endpoint>/metrics</endpoint>
    <port>9363</port>
    <metrics>true</metrics>
    <events>true</events>
    <asynchronous_metrics>true</asynchronous_metrics>
 </prometheus>
</clickhouse>' >/etc/clickhouse-server/config.d/prometheus.xml
```

-----------------------------------------------------------------------

# Clickhouse - Monitoring

<br>

* install prometheus/victoriametrics

```
VERSION=1.96.0
IP=$(hostname -I | awk '{print $2}')
wget -qq https://github.com/VictoriaMetrics/VictoriaMetrics/releases/download/v${VERSION}/victoria-metrics-linux-amd64-v${VERSION}.tar.gz
tar xzf victoria-metrics-linux-amd64-v${VERSION}.tar.gz -C /usr/local/bin/
chmod +x /usr/local/bin/victoria-metrics-prod
groupadd --system victoriametrics
useradd -s /sbin/nologin --system -g victoriametrics victoriametrics
mkdir -p /var/lib/victoriametrics/ /etc/victoriametrics/
chown victoriametrics:victoriametrics /var/lib/victoriametrics/ /etc/victoriametrics/
```

-----------------------------------------------------------------------

# Clickhouse - Monitoring

<br>

* install victoriametrics systemd service

```
echo "
[Unit]
Description=Description=VictoriaMetrics service
After=network.target

[Service]
Type=simple
LimitNOFILE=2097152
User=victoriametrics
Group=victoriametrics
ExecStart=/usr/local/bin/victoria-metrics-prod \
       -storageDataPath=/var/lib/victoriametrics/ \
       -httpListenAddr=0.0.0.0:8428 \
       -retentionPeriod=1 \
       -promscrape.config=/etc/victoriametrics/scrape.yml \
       -selfScrapeInterval=10s

SyslogIdentifier=victoriametrics
Restart=always

PrivateTmp=yes
ProtectHome=yes
NoNewPrivileges=yes
ProtectSystem=full

[Install]
WantedBy=multi-user.target
" >/etc/systemd/system/victoriametrics.service
```

-----------------------------------------------------------------------

# Clickhouse - Monitoring

<br>

* add configuration file with each clickhouse endpoints

```
echo "
global:
  external_labels:
    youtube: 'xavki'
scrape_configs:
  - job_name: node_exporter
    static_configs:
      - targets: 
" > /etc/victoriametrics/scrape.yml
awk '$1 ~ "^192.168" {print "        - "$2":9100"}' /etc/hosts >> /etc/victoriametrics/scrape.yml

echo "
  - job_name: clickhouse_metrics
    static_configs:
      - targets:" >> /etc/victoriametrics/scrape.yml

awk '$2 ~ "clickhouse" {print "        - "$2":9363"}' /etc/hosts >> /etc/victoriametrics/scrape.yml
```

-----------------------------------------------------------------------

# Clickhouse - Monitoring

<br>

* start & enable victoriametrics

```
  systemctl restart victoriametrics
  systemctl enable victoriametrics
```

-----------------------------------------------------------------------

# Clickhouse - Monitoring

<br>

* install grafana server

```
apt install gnupg2 curl software-properties-common dirmngr apt-transport-https lsb-release ca-certificates
wget -q -O /usr/share/keyrings/grafana.key https://apt.grafana.com/gpg.key
echo "deb [signed-by=/usr/share/keyrings/grafana.key] https://apt.grafana.com stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
apt-get update -qq >/dev/null
apt-get install -qq -y grafana >/dev/null
```

-----------------------------------------------------------------------

# Clickhouse - Monitoring

<br>

* then install dedicated clickhouse dashboard

```
https://raw.githubusercontent.com/weastur/grafana-dashboards/main/dashboards/clickhouse/clickhouse.json
```

* and node exporter dashboard

```
wget https://raw.githubusercontent.com/rfrail3/grafana-dashboards/master/prometheus/node-exporter-full.json
```
