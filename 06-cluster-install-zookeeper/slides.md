%title: Clickhouse
%author: xavki

 ██████╗██╗     ██╗ ██████╗██╗  ██╗██╗  ██╗ ██████╗ ██╗   ██╗███████╗███████╗
██╔════╝██║     ██║██╔════╝██║ ██╔╝██║  ██║██╔═══██╗██║   ██║██╔════╝██╔════╝
██║     ██║     ██║██║     █████╔╝ ███████║██║   ██║██║   ██║███████╗█████╗  
██║     ██║     ██║██║     ██╔═██╗ ██╔══██║██║   ██║██║   ██║╚════██║██╔══╝  
╚██████╗███████╗██║╚██████╗██║  ██╗██║  ██║╚██████╔╝╚██████╔╝███████║███████╗
 ╚═════╝╚══════╝╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝  ╚═════╝ ╚══════╝╚══════╝

-----------------------------------------------------------------------

# Clickhouse - Cluster : zookeeper installation

<br>

* install java and zookeeper

```
apt install openjdk-17-jre-headless zookeeper
```

-----------------------------------------------------------------------

# Clickhouse - Cluster : zookeeper installation

<br>

* configure zookeeper

```
autopurge.purgeInterval=1
autopurge.snapRetainCount=5
# To avoid seeks ZooKeeper allocates space in the transaction log file in blocks of preAllocSize kilobytes.
# The default block size is 64M. One reason for changing the size of the blocks is to reduce the block size
# if snapshots are taken more often. (Also, see snapCount).
preAllocSize=65536
# ZooKeeper logs transactions to a transaction log. After snapCount transactions are written to a log file a
# snapshot is started and a new transaction log file is started. The default snapCount is 10,000.
snapCount=10000
# allow command
4lw.commands.whitelist=stat
# define servers ip and internal ports to zookeeper
server.1=192.168.12.170:2888:3888
server.2=192.168.12.171:2888:3888
server.3=192.168.12.172:2888:3888
```

-----------------------------------------------------------------------

# Clickhouse - Cluster : zookeeper installation

<br>

* set the node id with myid file

```
echo 1 > /etc/zookeeper/conf/myid
```

-----------------------------------------------------------------------

# Clickhouse - Cluster : zookeeper installation

<br>

```
echo '[Unit]
Description=Apache Zookeeper Server
Documentation=http://kafka.apache.org/documentation.html
Requires=network.target
After=network.target zookeeper.service
[Service]
Type=forking
User=zookeeper
Group=zookeeper
ExecStart=/usr/share/zookeeper/bin/zkServer.sh start /etc/zookeeper/conf/zoo.cfg
ExecStop=/usr/share/zookeeper/bin/zkServer.sh stop /etc/zookeeper/conf/zoo.cfg
ExecReload=/usr/share/zookeeper/bin/zkServer.sh restart /etc/zookeeper/conf/zoo.cfg
Restart=on-abnormal
[Install]
WantedBy=multi-user.target' > /etc/systemd/system/zookeeper.service
```

