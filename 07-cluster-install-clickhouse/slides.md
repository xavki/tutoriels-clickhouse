%title: Clickhouse
%author: xavki

 ██████╗██╗     ██╗ ██████╗██╗  ██╗██╗  ██╗ ██████╗ ██╗   ██╗███████╗███████╗
██╔════╝██║     ██║██╔════╝██║ ██╔╝██║  ██║██╔═══██╗██║   ██║██╔════╝██╔════╝
██║     ██║     ██║██║     █████╔╝ ███████║██║   ██║██║   ██║███████╗█████╗  
██║     ██║     ██║██║     ██╔═██╗ ██╔══██║██║   ██║██║   ██║╚════██║██╔══╝  
╚██████╗███████╗██║╚██████╗██║  ██╗██║  ██║╚██████╔╝╚██████╔╝███████║███████╗
 ╚═════╝╚══════╝╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝  ╚═════╝ ╚══════╝╚══════╝

-----------------------------------------------------------------------

# Clickhouse - Cluster : clickhouse installation

<br>

Documentation : https://clickhouse.com/docs/en/architecture/replication

-----------------------------------------------------------------------

# Clickhouse - Cluster : clickhouse installation

<br>

* install clickhouse repository

```
apt install -y apt-transport-https ca-certificates dirmngr gnupg2
GNUPGHOME=$(mktemp -d)
GNUPGHOME="$GNUPGHOME" gpg --no-default-keyring --keyring /usr/share/keyrings/clickhouse-keyring.gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 8919F6BD2B48D754
rm -r "$GNUPGHOME"
chmod +r /usr/share/keyrings/clickhouse-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/clickhouse-keyring.gpg] https://packages.clickhouse.com/deb stable main" | sudo tee /etc/apt/sources.list.d/clickhouse.list
apt update
```

-----------------------------------------------------------------------

# Clickhouse - Cluster : clickhouse installation

<br>

* install clickhouse-server and client

```
DEBIAN_FRONTEND=noninteractive apt install -y clickhouse-server clickhouse-client
systemctl start clickhouse-server
clickhouse-client
```

-----------------------------------------------------------------------

# Clickhouse - Cluster : clickhouse installation

<br>

* configure clickhouse - zookeeper servers

```
echo '
<clickhouse>
    <zookeeper>
        <node>
            <host>zoo1</host>
            <port>2181</port>
        </node>
        <node>
            <host>zoo2</host>
            <port>2181</port>
        </node>
        <node>
            <host>zoo3</host>
            <port>2181</port>
        </node>
        <session_timeout_ms>30000</session_timeout_ms>
        <operation_timeout_ms>10000</operation_timeout_ms>
    </zookeeper>
    <distributed_ddl>
        <path>/clickhouse/task_queue/ddl</path>
    </distributed_ddl>
</clickhouse>
' >/etc/clickhouse-server/config.d/zookeeper.xml
```

-----------------------------------------------------------------------

# Clickhouse - Cluster : clickhouse installation

<br>

* configure the cluster

```
echo '
<clickhouse>
    <macros>
        <cluster>cluster_xavki</cluster>
        <shard>01</shard>
        <replica>2</replica>
    </macros>
</clickhouse>
' >/etc/clickhouse-server/config.d/macros.xml
```

-----------------------------------------------------------------------

# Clickhouse - Cluster : clickhouse installation

<br>

* configure the remote server

```
echo '
<clickhouse>
    <remote_servers>
        <cluster_xavki>
            <secret>password</secret>
	   				<shard>
              <internal_replication>true</internal_replication>
                  <replica>
                      <host>clickhouse1</host>
                      <port>9000</port>
                  </replica>
                  <replica>
                      <host>clickhouse2</host>
                      <port>9000</port>
                  </replica>
            </shard>
        </cluster_xavki>
    </remote_servers>
</clickhouse>
' >/etc/clickhouse-server/config.d/clusters.xml
```

-----------------------------------------------------------------------

# Clickhouse - Cluster : clickhouse installation

<br>

* listen on all interfaces

```
sed -i 's#<!-- <listen_host>::</listen_host> -->#<listen_host>::</listen_host>#g' /etc/clickhouse-server/config.xml
```

-----------------------------------------------------------------------

# Clickhouse - Cluster : clickhouse installation

<br>

* create a user

```
echo '
<clickhouse>
	<users>
		<xavki>
				<password>password</password>
        <access_management>1</access_management>

        <networks>
                <ip>::/0</ip>
        </networks>

        <profile>default</profile>

        <quota>default</quota>
        <default_database>default</default_database>
        <databases>
            <database_name>
                <table_name>
                    <filter>expression</filter>
                </table_name>
            </database_name>
        </databases>
    </xavki>
	</users>
</clickhouse>' >/etc/clickhouse-server/users.d/xavki.xml
```

-----------------------------------------------------------------------

# Clickhouse - Cluster : clickhouse installation

<br>

* restart servers

```
systemctl restart clickhouse-server

clickhouse-client --user xavki --password 
```

-----------------------------------------------------------------------

# Clickhouse - Cluster : clickhouse installation

<br>

* then test the replication - create a database and table

```
CREATE DATABASE db1 ON CLUSTER cluster_xavki

CREATE TABLE db1.table1 ON CLUSTER cluster_xavki
(
    `id` UInt64,
    `column1` String
)
ENGINE = ReplicatedMergeTree
ORDER BY id
```

-----------------------------------------------------------------------

# Clickhouse - Cluster : clickhouse installation

<br>

* create test rows

```
INSERT INTO db1.table1 (id, column1) VALUES (1, 'abc');
INSERT INTO db1.table1 (id, column1) VALUES (2, 'def');
INSERT INTO db1.table1 (id, column1) VALUES (3, 'ghi');
```
