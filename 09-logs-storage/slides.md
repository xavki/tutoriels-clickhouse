%title: Clickhouse
%author: xavki

 ██████╗██╗     ██╗ ██████╗██╗  ██╗██╗  ██╗ ██████╗ ██╗   ██╗███████╗███████╗
██╔════╝██║     ██║██╔════╝██║ ██╔╝██║  ██║██╔═══██╗██║   ██║██╔════╝██╔════╝
██║     ██║     ██║██║     █████╔╝ ███████║██║   ██║██║   ██║███████╗█████╗  
██║     ██║     ██║██║     ██╔═██╗ ██╔══██║██║   ██║██║   ██║╚════██║██╔══╝  
╚██████╗███████╗██║╚██████╗██║  ██╗██║  ██║╚██████╔╝╚██████╔╝███████║███████╗
 ╚═════╝╚══════╝╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝  ╚═════╝ ╚══════╝╚══════╝

-----------------------------------------------------------------------

# Clickhouse - Monitoring

<br>

* create table

```
clickhouse-client -h clickhouse1 --user xavki --password password -q "CREATE DATABASE fluentbit ON CLUSTER '{cluster}';"
clickhouse-client -h clickhouse1 --multiquery --user xavki --password password -q "
SET allow_experimental_object_type = 1;
CREATE TABLE IF NOT EXISTS fluentbit.jsonlogs
               (
                   timestamp DateTime,
                   log JSON
               )
ENGINE = ReplicatedMergeTree('/data/tables/jsonlogs', '{replica}')
ORDER BY tuple();
```

-----------------------------------------------------------------------

# Clickhouse - Monitoring

<br>

* install nginx and fluentbit

```
apt install gnupg2 nginx -y -qq 2>&1 >/dev/null
curl https://packages.fluentbit.io/fluentbit.key | gpg --dearmor > /usr/share/keyrings/fluentbit-keyring.gpg
curl https://raw.githubusercontent.com/fluent/fluent-bit/master/install.sh | sh
systemctl enable fluent-bit
systemctl start fluent-bit
```

-----------------------------------------------------------------------

# Clickhouse - Monitoring

<br>

* add fluentbit settings

```
echo "
[SERVICE]
    flush        1
    daemon       Off
    log_level    info
    parsers_file parsers.conf
    plugins_file plugins.conf
    http_server  Off
    http_listen  0.0.0.0
    http_port    2020
    storage.metrics on

[INPUT]
    name tail
    path /var/log/nginx/access.log
    read_from_head true
    parser nginx

[FILTER]
    Name nest
    Match *
    Operation nest
    Wildcard *
    Nest_under log 

[OUTPUT]
    name http
    tls off
    match *
    host clickhouse1
    port 8123
    URI /?query=INSERT+INTO+fluentbit.jsonlogs+FORMAT+JSONEachRow
    format json_stream
    json_date_key timestamp
    json_date_format epoch
    http_user xavki
    http_passwd password
">/etc/fluent-bit/fluent-bit.conf
```

-----------------------------------------------------------------------

# Clickhouse - Monitoring

<br>

* restart fluentbit

```
systemctl restart fluent-bit
```

-----------------------------------------------------------------------

# Clickhouse - Monitoring

<br>

* describe tables

```
DESCRIBE TABLE jsonlogs
FORMAT Vertical
SETTINGS describe_extend_object_types = 1
```

-----------------------------------------------------------------------

# Clickhouse - Monitoring

<br>

* a specific query for our logs

```
SELECT COUNT(*) FROM jsonlogs WHERE
timestamp BETWEEN '2024-01-30 21:05:57'
AND '2024-01-30 21:10:59'
AND log.agent ILIKE '%go-http%' ;
```
